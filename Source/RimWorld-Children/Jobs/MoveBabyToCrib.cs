﻿using Verse;
using Verse.AI;
using RimWorld;
using System.Collections.Generic;
using System.Diagnostics;

namespace RimWorldChildren
{

	public class WorkGiver_TakeBabyToCrib : WorkGiver_Scanner
	{
		//
		// Properties
		//
		public override PathEndMode PathEndMode {
			get {
				return PathEndMode.Touch;
			}
		}
		public override ThingRequest PotentialWorkThingRequest {
			get {
				return ThingRequest.ForGroup (ThingRequestGroup.Pawn);
			}
		}

		//
		// Methods
		//
		public override bool HasJobOnThing (Pawn pawn, Thing t, bool forced = false)
		{
			Pawn baby = t as Pawn;
			if (baby == null || baby == pawn) {
				return false;
			}
			if (!baby.RaceProps.Humanlike || baby.ageTracker.CurLifeStageIndex > AgeStage.Toddler) {
				return false;
			}
			if (!pawn.CanReserveAndReach (t, PathEndMode.ClosestTouch, Danger.Deadly, 1, -1, null, forced)) {
				return false;
			}
			// Baby is in a crib already
			if (baby.InBed() && ChildrenUtility.IsBedCrib(baby.CurrentBed() ) ){
				return false;
			}
			if(baby.ageTracker.CurLifeStageIndex == AgeStage.Toddler){
				return false;
			}
			// Is it time for the baby to go to bed?
			bool baby_sleep_time = baby.timetable.GetAssignment(GenLocalDate.HourInteger(baby.Map)).allowRest;
			if(!baby_sleep_time){
				return false;
			}
			Building_Bed crib = ChildrenUtility.FindCribFor(baby, pawn);
			if ( crib == null ){
				JobFailReason.Is("NoCrib".Translate());
				return false;
			}
			return true;
		}
		
		public override Job JobOnThing(Pawn pawn, Thing t, bool forced = false)
		{
			var baby = (Pawn)t;
			Building_Bed crib = ChildrenUtility.FindCribFor(baby, pawn);
			if (baby != null && crib != null){
				Job carryBaby = new Job(DefDatabase<JobDef>.GetNamed("TakeBabyToCrib"), baby, crib){ count = 1 };
				return carryBaby;
			}
			return null;
		}
	}
}